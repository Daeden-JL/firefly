#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# This script requires the following aruguments to run:
# Address Webhook
# It can also take the following optional aruguments:
# Notification-Delay-Minutes Check-Interval
# Example 192.168.1.1 webhook 5m 30s

LDM = "2021-07-25"
VERSION = "0.0.1"

import socket
import sys
import json
import requests

print(str(sys.argv))


def icmp_check(server):
    s = socket.socket()
    check = s.connect_ex((server, port))
    s.detach()
    return check

def webhook_send(status, webhook):
