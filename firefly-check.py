#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# This script requires the following aruguments to run:
# Address Port Webhook
# It can also take the following optional aruguments:
# Notification-Delay-Minutes Check-Interval
# Example 192.168.1.1 22 webhook 5m 30s

LDM = "2021-08-03"
VERSION = "0.0.1"

import socket
import sys
import json
import requests

print(str(sys.argv))

def tcp_port_check(server, port):
    '''Checks a given TCP port on a given server and returns the results.'''
    s = socket.socket()
    check = s.connect_ex((server, port))
    s.detach()
    return check

def webhook_send(status, webhook):
    '''Sends a given text status to a given webhook.
    Raises any error code that success (200).'''
    data = {'text': status}
    formatted_data = json.dumps(data)
    headers = {'Content-Type': 'application/json'}
    response = requests.post(webhook, formatted_data, headers)
    if response.status_code != 200:
        raise ValueError('Webhook returned an error %s, the response is:\n%s' % (response.status_code, response.text))

# Setting up variables
if len(sys.argv) > 6:
    print('You have specified too many arguments')
    sys.exit()

if len(sys.argv) < 4:
    print('You need to specify the Address, Port, and Webhook.')
    sys.exit()

server = str(sys.argv[1])
port = int(sys.argv[2])
webhook = str(sys.argv[3])
if len(sys.argv) > 4:
    delay = str(sys.argv[4])

if len(sys.argv) > 5:
    interval = int(sys.argv[5])

down = 0
up = 0

#while True:
#    '''This loop is designed to continue until the pod dies.'''

# Single run for testing
status = tcp_port_check(server, port)
webhook_send(status, webhook)
